# Baixo Imobiliario

Um jogo geo-based criado pela Transparência Hacker para o festival Baixo Centro de 2012
 
## Instalação

O Baixo Imobiliario depende da biblioteca Flask para rodar:

*  easy_install flask

Também é necessario criar o arquivo settings.py na raiz do diretorio apontando para o banco de dados.
Um arquivo de exemplo esta em settings-sample e um banco de dados de exemplo esta na pasta data/lambe.sql.
