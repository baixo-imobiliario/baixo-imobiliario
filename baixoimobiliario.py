import sqlite3
from flask import Flask, render_template, request, session, g, flash
from contextlib import closing
import tools
   
def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])
            
app = Flask(__name__)
app.config.from_object('settings')

@app.before_request
def before_request():
    g.db = connect_db()
    g.db.row_factory = tools.dict_factory

@app.teardown_request
def teardown_request(exception):
    g.db.close()
    
@app.route("/")
def index():
    cur = g.db.execute('SELECT * FROM lambes')
    lambes = cur.fetchall()
    return render_template('index.html', lambes=lambes)

@app.route("/lambe/<endereco_slug>")
def lambe(endereco_slug):
    cur = g.db.execute('SELECT * FROM lambes WHERE (endereco_slug) == (?)', [endereco_slug])
    lambes = cur.fetchone()
    if lambes:
        if session.has_key(endereco_slug):
            #if not first time!
            return render_template('lambe.html', lambes=lambes)
        else:
            #if first time!
            session[endereco_slug] = True
            g.db.execute('UPDATE lambes SET occupy_count=(?) where (endereco_slug) == (?)', [lambes['occupy_count']+1, endereco_slug])            
            g.db.commit()
            cur = g.db.execute('SELECT * FROM lambes WHERE (endereco_slug) == (?)', [endereco_slug])
            lambes = cur.fetchone()
            return render_template('lambe.html', lambes=lambes)
    else:
        return 'Rua inexistente!', 404
        

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
        
if __name__ == "__main__":
    app.run(debug=True)
